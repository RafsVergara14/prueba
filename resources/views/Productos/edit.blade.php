<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">



<form action="{{url('/Productos/' . $Productos->id)}}" method="post" enctype="multipart/form-data" class="row g-3">
{{csrf_field()}} 
{{method_field('PATCH')}}


  <div class="col-md-6">
    <label for="inputEmail4" class="form-label">Nombre</label>
    <input type="text" class="form-control" name="Nombre_Producto" id="Nombre" value="{{$Productos->Nombre_Producto}}">
  </div>
  <div class="col-md-6">
    <label for="inputPassword4" class="form-label">Referencia</label>
    <input type="text" class="form-control" name="Referencia" id="Referencia" value="{{$Productos->Referencia}}">
  </div>
  <div class="col-12">
    <label for="inputAddress" class="form-label">Precio</label>
    <input type="number" class="form-control" name="Precio" id="Precio" placeholder="Col" value="{{$Productos->Precio}}">
  </div>
  <div class="col-12">
    <label for="inputAddress2" class="form-label">Peso</label>
    <input type="number" class="form-control" name="Peso" id="Peso" placeholder="Gramos" value="{{$Productos->Peso}}">
  </div>
  <div class="col-md-6">
    <label for="inputCity" class="form-label">Categoria</label>
    <input type="text" class="form-control" name="Categoria" id="Categoria" value="{{$Productos->Categoria}}">
  </div>
  <div class="col-12">
    <label for="inputAddress2" class="form-label">Stock</label>
    <input type="number" class="form-control" name="Stock" id="Stock" placeholder="Apartment, studio, or floor" value="{{$Productos->Stock}}">
  </div>
  <div class="col-12">
    <label for="inputAddress2" class="form-label">Fecha de Creacion</label>
    <input type="date" class="form-control" name="Fecha_create" id="fechaCrecaion" placeholder="Apartment, studio, or floor" value="{{$Productos->Fecha_create}}">
  </div>  
  <div class="col-12">
    <label for="inputAddress2" class="form-label">Fecha Ultima Venta</label>
    <input type="date-time" class="form-control" name="Fecha_ultima_venta" id="Fecha_ultima_venta" placeholder="Apartment, studio, or floor" value="{{$Productos->Fecha_ultima_venta}}">
  </div>  





  <div class="col-12">
    <button type="submit" class="btn btn-primary">Actualizar</button>
  </div>
</form>

</html>