
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">


<h1>Prodcutos en venta</h1>
<a href="{{ url('Productos/create')}}"  class="btn btn-success"> Crear Nuevo Producto</a>
<table id="tablas" class="table table-light table-hover" style="width:100%">
    <thead>
        <tr>
            <th>ID</th>
            <th>NOMBRE PRODUCTO</th>
            <th>REFERENCIA</th>
            <th>PRECIO</th>
            <th>PESO</th>
            <th>CATEGORIA</th>
            <th>STOCK</th>
            <th>FECHA DE CREACION</th>
            <th>FECHA DE VENTA</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach($Productos as $Productos)
        <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{$Productos->Nombre_Producto}}</td>
            <td>{{$Productos->Referencia}}</td>
            <td>{{$Productos->Precio}}</td>
            <td>{{$Productos->Peso}}</td>
            <td>{{$Productos->Categoria}}</td>
            <td>{{$Productos->Stock}}</td>
            <td>{{$Productos->Fecha_create}}</td>
            <td>{{$Productos->Fecha_ultima_venta}}</td>
            <td>
            <a href="{{url('/Productos/'.$Productos->id.'/edit')}}" class="btn btn-warning">Editar</a>
            <form method="post" action="{{url('/Productos/'.$Productos->id)}}" style="display:inline">
            {{csrf_field()}}
            {{method_field('DELETE')}}
            <button type="submit" onclick="return confirm('¿Borrar?');" class="btn btn-danger">Borrar</button>
            </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
</html>