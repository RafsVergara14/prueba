
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">


<form action="{{ url('/Productos')}}" method="post" enctype="multipart/form-data" class="row g-3">
{{ csrf_field()}}


  <div class="col-md-6">
    <label for="inputEmail4" class="form-label">Nombre</label>
    <input type="text" class="form-control" name="Nombre_Producto" id="Nombre" required>
  </div>
  <div class="col-md-6">
    <label for="inputPassword4" class="form-label">Referencia</label>
    <input type="text" class="form-control" name="Referencia" id="Referencia" required>
  </div>
  <div class="col-12">
    <label for="inputAddress" class="form-label">Precio</label>
    <input type="number" class="form-control" name="Precio" id="Precio" placeholder="Col" required>
  </div>
  <div class="col-12">
    <label for="inputAddress2" class="form-label">Peso</label>
    <input type="number" class="form-control" name="Peso" id="Peso" placeholder="Gramos" required>
  </div>
  <div class="col-md-6">
    <label for="inputCity" class="form-label">Categoria</label>
    <input type="text" class="form-control" name="Categoria" id="Categoria" required>
  </div>
  <div class="col-12">
    <label for="inputAddress2" class="form-label">Stock</label>
    <input type="number" class="form-control" name="Stock" id="Stock" placeholder="Cantidad Actualmente" required>
  </div>
  <div class="col-12">
    <label for="inputAddress2" class="form-label">Fecha de Creacion</label>
    <input type="date" class="form-control" name="Fecha_create" id="fechaCrecaion" required>
  </div>  
  <div class="col-12">
    <label for="inputAddress2" class="form-label">Fecha de CreaciUltima Venta</label>
    <input type="datetime-local" class="form-control" name="Fecha_ultima_venta" id="FechaVenta" required>
  </div>  

  <div class="col-12">
    <button type="submit" class="btn btn-primary">Enviar</button>
  </div>
</form>

</html>